import React from 'react';

const List = () => {
    return (
        <div className="flex justify-between items-center">
            <div className="blogs bg-white mr-5">
                <img
                    src="https://images.unsplash.com/photo-1489396160836-2c99c977e970?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
                    className=""
                />
                <div className="p-5">
                    <h1 className="text-2xl font-bold text-green-800 py-2">
                        Lorem ipsum dolor sit amet
                    </h1>
                    <p className="bg-white text-sm text-black">
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit. Reiciendis vitae qui distinctio ex soluta?
                        Voluptates, ea! Esse, natus. Quas possimus laboriosam
                        consectetur deserunt ea sapiente. Dicta ipsam atque
                        voluptatem provident!
                    </p>
                    <a
                        href=""
                        className="py-2 px-3 mt-4 px-6 text-white bg-green-500 inline-block rounded">
                        Read More
                    </a>
                </div>
            </div>

            <div className="blogs bg-white mr-5">
                <img
                    src="https://images.unsplash.com/photo-1489396160836-2c99c977e970?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
                    className=""
                />
                <div className="p-5">
                    <h1 className="text-2xl font-bold text-green-800 py-2">
                        Lorem ipsum dolor sit amet
                    </h1>
                    <p className="bg-white text-sm text-black">
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit. Reiciendis vitae qui distinctio ex soluta?
                        Voluptates, ea! Esse, natus. Quas possimus laboriosam
                        consectetur deserunt ea sapiente. Dicta ipsam atque
                        voluptatem provident!
                    </p>
                    <a
                        href=""
                        className="py-2 px-3 mt-4 px-6 text-white bg-green-500 inline-block rounded">
                        Read More
                    </a>
                </div>
            </div>

            <div className="blogs bg-white mr-5">
                <img
                    src="https://images.unsplash.com/photo-1489396160836-2c99c977e970?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
                    className=""
                />
                <div className="p-5">
                    <h1 className="text-2xl font-bold text-green-800 py-2">
                        Lorem ipsum dolor sit amet
                    </h1>
                    <p className="bg-white text-sm text-black">
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit. Reiciendis vitae qui distinctio ex soluta?
                        Voluptates, ea! Esse, natus. Quas possimus laboriosam
                        consectetur deserunt ea sapiente. Dicta ipsam atque
                        voluptatem provident!
                    </p>
                    <a
                        href=""
                        className="py-2 px-3 mt-4 px-6 text-white bg-green-500 inline-block rounded">
                        Read More
                    </a>
                </div>
            </div>
        </div>
    );
};

export default List;
