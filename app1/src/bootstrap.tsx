import React from 'react';
import ReactDom from 'react-dom';
import App from './App';

import '@fontsource/open-sans';

ReactDom.render(<App single={true} />, document.getElementById('app'));