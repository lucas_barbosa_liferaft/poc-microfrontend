const path = require('path');
const { merge } = require('webpack-merge');
const ModuleFederationPlugin =
    require('webpack').container.ModuleFederationPlugin;

const baseConfig = require('./base.config');
const deps = require('../package.json').dependencies;

module.exports = merge(baseConfig, {
    devtool: 'eval-source-map',

    devServer: {
        contentBase: path.resolve(__dirname, '../src'),
        port: 3001,
        inline: true,
        hot: true,
        hotOnly: true,
        historyApiFallback: {
            disableDotRule: true,
        },
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods':
                'GET, POST, PUT, DELETE, PATCH, OPTIONS',
            'Access-Control-Allow-Headers':
                'X-Requested-With, content-type, Authorization',
        },
    },
    plugins: [
        new ModuleFederationPlugin({
            name: 'app1',
            filename: 'remoteEntry.js',
            remotes: {
                base: 'base@http://localhost:3000/remoteEntry.js',
                app1: 'app1@http://localhost:3001/remoteEntry.js',
                app2: 'app2@http://localhost:3002/remoteEntry.js',
            },
            shared: {
                ...deps,
                react: {
                    singleton: true,
                    requiredVersion: deps.react,
                },
                'react-dom': {
                    singleton: true,
                    requiredVersion: deps['react-dom'],
                },
                'react-router-dom': {
                    singleton: true,
                    requiredVersion: deps['react-router-dom'],
                }
            },
            exposes: {
                './App': './src/App',
                './AppBadge': './src/components/AppBadge',
            },
        }),
    ],
});
