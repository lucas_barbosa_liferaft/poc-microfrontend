const path = require('path');
const { merge } = require('webpack-merge');
const ModuleFederationPlugin =
    require('webpack').container.ModuleFederationPlugin;

const baseConfig = require('./base.config');

module.exports = merge(baseConfig, {
    devtool: 'eval-source-map',

    devServer: {
        contentBase: path.resolve(__dirname, '../src'),
        port: 3001,
        inline: true,
        open: true,
        hot: true,
        hotOnly: false,
        historyApiFallback: {
            disableDotRule: true,
        },
    },
    plugins: [
        new ModuleFederationPlugin({
            name: 'app1',
            filename: 'remoteEntry.js',
            exposes: {
                './App': './src/App',
                './AppBadge': './src/components/AppBadge',
            },
            shared: ['react', 'react-dom', 'react-router-dom'],
        }),
    ],
});
