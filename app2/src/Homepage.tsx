import React from 'react';
import Card from './components/Card';
import { Content } from './types';

interface HomepageProps {
    contents: Content[];
}

export const Homepage: React.FC<HomepageProps> = ({ contents }) => {
    const list = contents.map(({ id, description, image, title, link }) => (
        <Card
            key={id}
            description={description}
            title={title}
            image={image}
            link={link}
        />
    ));

    return <div className="p-6 grid grid-rows-1 gap-6 bg-red-200">{list}</div>;
};
