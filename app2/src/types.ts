export type Content = {
    id: number;
    title: string;
    description: string;
    text: string;
    link: string;
    image: string;
    avatar: string;
};
