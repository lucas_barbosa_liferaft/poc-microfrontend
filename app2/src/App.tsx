import React from 'react';
import { BrowserRouter, useLocation, useRoutes } from 'react-router-dom';
import faker from 'faker';
import './App.css';
import { Homepage } from './Homepage';
import { Content } from './types';
import Page from './components/Page';

import data from './data.json';

interface RouteProps {
    path: string;
}

// const getRandomRangeNumber = (min: number = 1, max: number = 5) => {
//     return Math.random() * (max - min) + min;
// };

// const data = new Array(10).fill(0).map((v, i) => {
//     const title = faker.lorem.words(getRandomRangeNumber());
//     return {
//         id: i + 1,
//         title,
//         description: faker.lorem.paragraph(),
//         text: faker.lorem.text(getRandomRangeNumber()),
//         link: faker.helpers.slugify(title),
//         image: faker.image.imageUrl(800, 500),
//         avatar: faker.image.people(40, 40),
//     };
// });

// console.log(JSON.stringify(data))

const Routes: React.FC<RouteProps> = ({ path = '' }) => {
    const contents: Content[] = data;

    const contentsRoutes = contents.map((content) => {
        return {
            path: `/${content.link}`,
            element: <Page {...content} />,
        };
    });

    const routes = useRoutes(
        [
            {
                path: `/`,
                element: <Homepage contents={contents} />,
            },
            ...contentsRoutes,
            {
                path: `*`,
                element: <div>Nothing to show</div>,
            },
        ],
    );

    return routes;
};

interface AppProps {
    single?: boolean;
    path?: string;
}
const App: React.FC<AppProps> = ({ single = false, path = '' }) => {
    return single ? (
        <BrowserRouter>
            <Routes path={path} />
        </BrowserRouter>
    ) : (
        <Routes path={path} />
    );
};

export default App;
