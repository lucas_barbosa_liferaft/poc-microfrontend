import React from 'react';
import { useNavigate } from 'react-router-dom';
import '../App.css';

interface AppBadgeProps {
    path?: string;
}

const AppBadge: React.FC<AppBadgeProps> = ({ path = '/' }) => {
    const navigate = useNavigate();

    return (
        <div className="bg-white p-8 rounded-lg shadow-lg relative hover:shadow-2xl transition duration-500">
            <h1 className="text-2xl text-gray-800 font-semibold mb-3">App 2</h1>
            <button
                onClick={() => navigate(path, { replace: true })}
                className="py-2 px-4 mt-8 bg-indigo-600 text-white rounded-md shadow-xl">
                Learn More
            </button>
        </div>
    );
};

export default AppBadge;
