import React from 'react';
import { Link } from 'react-router-dom';
import { Content } from '../types';

const Page: React.FC<Content> = ({ avatar, text, title }) => {
    return (
        <div className="p-6 bg-gray-200">
            <div className="mb-6">
                <Link
                    to="/"
                    className="bg-blue-500 font-bold text-white px-4 py-3 transition duration-300 ease-in-out hover:bg-blue-600 mr-6">
                    Back
                </Link>
            </div>
            <div className="max-w-4xl px-10 py-6 mx-auto bg-white rounded-lg shadow-md">
                <div className="mt-2">
                    <h1 className="text-2xl font-bold text-gray-700 hover:underline">
                        {title}
                    </h1>
                    <p className="mt-2 text-gray-600">{text}</p>
                </div>
                <div className="flex items-center justify-between mt-4">
                    <div>
                        <a href="#" className="flex items-center">
                            <img
                                src={avatar}
                                alt="avatar"
                                className="hidden object-cover w-10 h-10 mx-4 rounded-full sm:block"
                            />
                            <h1 className="font-bold text-gray-700 hover:underline">
                                Alex John
                            </h1>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Page;
