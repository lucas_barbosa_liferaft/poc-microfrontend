import React from 'react';
import { Link } from 'react-router-dom';

interface CardProps {
    image: string;
    title: string;
    description: string;
    link: string;
}

const Card: React.FC<CardProps> = ({ title, description, link, image }) => {
    return (
        <div className="blogs bg-white mr-5">
            <img src={image} className="" />
            <div className="p-5">
                <h1 className="text-2xl font-bold text-green-800 py-2">
                    {title}
                </h1>
                <p className="bg-white text-sm text-black">{description}</p>
                <Link
                    to={link}
                    className="py-2 mt-4 px-6 text-white bg-blue-500 inline-block rounded">
                    Read More
                </Link>
            </div>
        </div>
    );
};

export default Card;
