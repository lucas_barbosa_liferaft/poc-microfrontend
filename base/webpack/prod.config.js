const path = require('path');
const { merge } = require('webpack-merge');
const ModuleFederationPlugin =
  require("webpack").container.ModuleFederationPlugin;

const baseConfig = require('./base.config');

module.exports = merge(baseConfig, {
    devtool: 'eval-source-map',

    devServer: {
        contentBase: path.resolve(__dirname, '../src'),
        port: 3000,
        inline: true,
        open: true,
        hot: true,
        hotOnly: false,
    },

    plugins: [
        new ModuleFederationPlugin({
            name: 'base',
            remotes: {
                app1: 'app1@http://localhost:3001/remoteEntry.js',
                app2: 'app2@http://localhost:3002/remoteEntry.js',
            },
            shared: ['react', 'react-dom'],
        }),
    ],
});
