const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const isProduction = process.env.NODE_ENV === 'production';
const isDevelopment = !isProduction;

const publicPath = process.env.PUBLIC_URL || '/';
const appSrc = path.resolve(__dirname, '../src');

module.exports = {
    target: isDevelopment ? 'web' : 'browserslist',
    mode: isDevelopment ? 'development' : 'production',
    bail: isProduction,
    entry: appSrc,
    output: {
        publicPath,
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    isProduction && MiniCssExtractPlugin.loader,
                    require.resolve('style-loader'),
                    {
                        loader: require.resolve('css-loader'),
                        options: {
                            importLoaders: 1,
                            sourceMap: isDevelopment,
                            modules: {
                                mode: 'icss',
                            },
                        },
                    },
                    {
                        loader: require.resolve('postcss-loader'),
                        options: {
                            postcssOptions: {
                                ident: 'postcss',
                                plugins: [
                                    'postcss-flexbugs-fixes',
                                    [
                                        'postcss-preset-env',
                                        {
                                            autoprefixer: {
                                                flexbox: 'no-2009',
                                            },
                                            stage: 3,
                                        },
                                    ],
                                    'postcss-normalize',
                                ],
                            },
                            sourceMap: isDevelopment,
                        },
                    },
                ].filter(Boolean),
                sideEffects: true,
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/i,
                type: 'asset/resource',
            },
            {
                test: /\.[jt]sx?$/,
                include: appSrc,
                use: [
                    {
                        loader: 'ts-loader',
                        options: {
                            configFile: path.resolve(
                                __dirname,
                                '../tsconfig.json',
                            ),
                            transpileOnly: true,
                        },
                    },
                    'astroturf/loader',
                ].filter(Boolean),
            },
        ],
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js', '.jsx'],
    },
    plugins: [
        new MiniCssExtractPlugin(),
        new HtmlWebpackPlugin({
            inject: true,
            filename: './index.html',
            template: path.resolve(__dirname, '../public/index.html'),
        }),
        new ForkTsCheckerWebpackPlugin(),
        new ESLintPlugin({
            context: path.resolve(__dirname, '../.'),
        }),
    ].filter(Boolean),
};
