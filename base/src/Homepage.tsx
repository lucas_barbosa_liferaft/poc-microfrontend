import React, { Suspense } from 'react';
const App1Badge = React.lazy(() => import('app1/AppBadge'));
const App2Badge = React.lazy(() => import('app2/AppBadge'));

const Homepage = () => {
    return (
        <div className="p-6 grid grid-rows-1 gap-6">
            <Suspense fallback={<></>}>
                <App1Badge path="app-1"/>
            </Suspense>
            <Suspense fallback={<></>}>
                <App2Badge path="app-2"/>
            </Suspense>
        </div>
    );
};

export default Homepage;
