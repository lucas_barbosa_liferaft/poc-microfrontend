import React, { Suspense } from 'react';
import { Link, useRoutes } from 'react-router-dom';
import Homepage from './Homepage';
const App1 = React.lazy(() => import('app1/App'));
const App2 = React.lazy(() => import('app2/App'));

const App: React.FC = () => {
    const routes = useRoutes([
        {
            path: '/',
            element: <Homepage />
        },
        {
            path: '/app-1/*',
            element: (
                <Suspense fallback="Loading...">
                    <App1 path="/app-1" />
                </Suspense>
            ),
        },
        {
            path: '/app-2/*',
            element: (
                <Suspense fallback="Loading...">
                    <App2 path="/app-2" />
                </Suspense>
            ),
        },
    ]);

    return (
        <div className="font-sans min-w-screen h-screen grid grid-rows-[auto,1fr]">
            <div className="flex-1 flex flex-col sticky">
                <nav className="px-4 flex justify-between bg-white h-16 border-b-2">
                    <ul className="flex items-center">
                        <li className="h-6 w-6">
                            <img
                                className="h-full w-full mx-auto"
                                src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Svelte_Logo.svg/512px-Svelte_Logo.svg.png"
                                alt="svelte logo"
                            />
                        </li>
                    </ul>

                    <ul className="flex items-center">
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                    </ul>

                    <ul className="flex items-center">
                        <li className="pr-6">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                stroke="currentColor"
                                strokeWidth="2"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                className="feather feather-bell">
                                <path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9" />
                                <path d="M13.73 21a2 2 0 0 1-3.46 0" />
                            </svg>
                        </li>
                        <li className="h-10 w-10">
                            <img
                                className="h-full w-full rounded-full mx-auto"
                                src="https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
                                alt="profile woman"
                            />
                        </li>
                    </ul>
                </nav>
            </div>
            <div className="overflow-auto">{routes}</div>
        </div>
    );
};

export default App;
