/// <reference types="react" />

declare module 'app1/AppBadge' {
    const AppBadge: React.ComponentType<{ path: string }>;

    export default AppBadge;
}

declare module 'app1/App' {
    const App: React.ComponentType<{ path: string }>;

    export default App;
}


declare module 'app2/AppBadge' {
    const AppBadge: React.ComponentType<{ path: string }>;

    export default AppBadge;
}

declare module 'app2/App' {
    const App: React.ComponentType<{ path: string }>;

    export default App;
}
