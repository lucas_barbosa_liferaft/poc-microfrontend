import React from 'react';
import ReactDom from 'react-dom';
import App from './App';
import './App.css';

import '@fontsource/open-sans';
import { BrowserRouter } from 'react-router-dom';

ReactDom.render(
    <BrowserRouter>
        <App />
    </BrowserRouter>,
    document.getElementById('app'),
);
